/**
 * 
 * 
 * 
 * 
 * for( khởi tạo ;   điều kiện ;  bước nhảy )
 * {
 *       hành động
 * }
 * 
 // *  phân biệt giữ vòng lặp và lần lặp
 // * khởi tạo : chỉ được chạy 1 lần duy nhất ở lần lặp đầu tiên
 //  ở lần lần lặp đầu : khởi tạo => điều kiện => hành động => bước nhẩy
 //  ở lần lặp thứ 2 trờ đi:điều kiện => hành động => bước nhẩy
 */
for (var i = 0; i <= 10; i = i + 2) {
  console.log("hello", i);
}
