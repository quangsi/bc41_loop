function inKetQua() {
  var number = document.getElementById("txt-number").value * 1;
  var count = 0;
  var contentHTML = "";
  while (number > 1) {
    count++;
    number = Math.floor(number / 2);
    var content = `<p>Count : ${count} - Number : ${number}</p>`;
    contentHTML += content;
  }
  document.getElementById("result").innerHTML = contentHTML;
}
